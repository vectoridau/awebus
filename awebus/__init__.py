#!/usr/bin/env python

from ._event_mixin import EventMixin
from ._event_bus import EventBus

# Class Aliases
BusMixin = EventMixin
Bus = EventBus
