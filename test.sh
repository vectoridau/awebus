#!/bin/sh

PYTHON_VERSIONS="3.4.0 3.4.1 3.5.0 3.5.1 3.6.0 3.6.1 3.7.0 3.7.1"

for PYTHON_VERSION in $PYTHON_VERSIONS
do
	echo "Running tests for Python $PYTHON_VERSION"
	PYENV_VERSION=$PYTHON_VERSION python -m unittest discover -s test -p '*.py'
	echo ""
	echo ""
done
